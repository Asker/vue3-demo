
# vue通用后台管理系统

基于vue3的一套简约版后台管理模板

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## 安装

```sh
npm install
```

### 运行

```sh
npm run dev
```

### 打包

```sh
npm run build
```
