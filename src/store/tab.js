import { createStore } from 'vuex';
export default createStore({
    // 存储数据
    state: {
        currentMenu: null,
        tabsList: [{
            path: '/',
            name: 'home',
            meta: { title: '首页', icon: 'Document' }
        }]
    },

    // 调用方法
    mutations: {
        // 选择标签 选择面包屑
        selectMenu(state, val) {
            if (val.name === 'home') {
                state.currentMenu = null;
            } else {
                state.currentMenu = val;
                // 如果等于-1,说明tabsList不存在那么插入,否则什么都不做
                let result = state.tabsList.findIndex(item => item.name === val.name);
                result === -1 ? state.tabsList.push(val) : '';
            }
        },

        // 关闭标签
        closeTab(state, val) {
            let tabList = JSON.parse(sessionStorage.getItem('tabsList'));
            let result = tabList.findIndex(item => item.name === val.name);
            state.tabsList.splice(result, 1);
            tabList = state.tabsList;
            sessionStorage.setItem('tabsList', JSON.stringify(state.tabsList));
        }
    },
    actions: {}
});