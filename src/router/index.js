import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        redirect: '/home',
        component: () => import('@/views/Main.vue'),
        children:[
            {
                path: '/home',
                name: 'home',
                component: () => import('@/views/Home.vue'),
                meta: { title: '首页',icon:'Document' }  // 配置路径别名后，可以使用@
            },
            {
                path: '/setting',
                name: 'setting',
                // component: () => import('@/views/Setting.vue'),  // 配置路径别名后，可以使用@
                redirect: '/setting/user',
                meta: { title: '系统设置' },
                children: [
                    {
                        path: 'user',
                        name: 'user',
                        component: () => import('@/views/UserMange.vue'),
                        meta: { title: '用户管理' } // 配置路径别名后，可以使用@
        
                    },
                    {
                        path: 'message',
                        name: 'message',
                        component: () => import('@/views/MesMange.vue'),
                        meta: { title: '短信管理' } // 配置路径别名后，可以使用@
        
                    }
                ],
        
            },
            {
                path: '/example',
                name: 'example',
                component: () => import('@/views/Example.vue'),
                meta: { title: '综合案例',icon:'IconMenu' }
            },
            {
                path: '/other',
                name: 'other',
                component: () => import('@/views/Other.vue'),
                meta: { title: '其他',icon:'Setting' }
            },
            
        ],
        
    },
    {
        path:'/setting/home',
        redirect:'/home'
    },
    {
        path:'/setting/example',
        redirect:'/example'
    },
    {
        path:'/setting/other',
        redirect:'/other'
    }
    
]
const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
